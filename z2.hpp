#include "iostream"

#define N 100

using namespace std;

struct SElement;

class CList
{
private:
	double a[N]; // сам массив
	double max; // максимальное значение в массиве
	double min; // минимальное значение в массиве
	int ind[N]; // индексирует, где находится элемент
	CList *next; // след. элемент списка
	CList *previous; // предыдущий элемент списка
	int base_ind;
	double base_value;
public:
	CList(void);
	CList(const CList &); //copiruem
	CList & operator=(const CList &);
	CList(int Length); // создать массив заданной начальной длины
	~CList(void);
	void Add(double x, int j); //vstavit' element s ukaz indexom
	void AddEnd(double x);//dobavit' element v konez
	void Delete(int j);//udalit' s ukaz ind
	void Delete(double x);
	double & GetS(int i); // nahodim ssilku
	int GetCount();
	int GetLength();
	void SetLength(int Length);
	SElement Min(); //index minimalnogo
//	double & operator[](const CList &);
    double & GetX(double x);
    CList Sort(void); // sortiruem
};
// CList a(100);
// a[1];
/*
указатель- где лежит обьект &(a[i]) = a+i
ссылка-сам обьект a[1]=b *(a+1)
индекс - i a[i] == *(a+i)

//*/

struct SElement
{
	int no;
	CList *where;
};
