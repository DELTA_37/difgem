all: prog

prog: main.o z2.o
	g++ main.o z2.o -g -o prog
main.o: main.cpp z2.hpp
	g++ main.cpp -c -g -o main.o
z2.o: z2_1.cpp z2.hpp
	g++ z2_1.cpp -c -g -o z2.o
clean:
	rm prog
