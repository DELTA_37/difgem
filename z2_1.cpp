#include "z2.hpp"
#include "float.h"

CList::~CList(void)
{
    if (this->next != NULL)
    delete this->next;
}

CList::CList(void)
{
	int i;
	this->base_ind = -1;
	this->base_value = -1;
	this->previous = NULL;
	this->next = NULL;
	this->max = DBL_MIN;
	this->min = DBL_MAX;
	for (i = 0; i <= N - 1; i++)
	{
		a[i] = 0;
		ind[i] = 0;
	}
}

CList::CList(int Length)
{
	int i;
	this->max = DBL_MIN;
	this->min = DBL_MAX;
	this->next = NULL;
	this->previous = NULL;
	this->base_ind = -1;
	this->base_value = -1;

	for (i = 0; i <= N - 1; i++)
	{
		a[i] = 0;
		ind[i] = 0;
	}
	if (Length >= N)
	{
		this->next = new CList(Length - N);
		this->next->previous = this; /*аксиома списка
					       следующий ->
					       предыдущий =
					       этот */
	}
}


void CList::Add(double x, int j)
{
	if ((j >= N)&&(this->next != NULL))
	{
		this->next->Add(x, j - N);
	}
	else if ((j >= N)&&(this->next == NULL))
	{
		this->next = new CList();
		this->next->previous = this;
		this->next->Add(x, j - N);
	}
	else if (j < N)
	{
		this->ind[j] = 1;
		this->a[j] = x;
		if (x > this->max)
		{
			this->max = x;
		}
		if (x < this->min)
		{
			this->min = x;
		}
	}
}

void CList::AddEnd(double x)
{
	if (this->next != NULL)
	{
		this->next->AddEnd(x);
	}
	else
	{
		int i;
		int j = -1;
		for (i = 0; i <= N - 1; i++)
		{
			if (ind[i] == 1)
			{
				j = i;
			}
		}
		this->Add(x, j + 1);
	}
}

void CList::Delete(int j)
{
    int i;
	if (j >= N)
	{
		this->next->Delete(j - N);
	}
	else
	{
	    this->ind[j] = 0;
		if ((a[j] <= max)&&(a[j] >= max))
		{
			double m1 = DBL_MIN;
			for (i = 0; i <= N - 1; i++)
			{
				if ((this->ind[i] == 1)&&(m1 < this->a[i]))
				{
					m1 = this->a[i];
				}
			}
			this->max = m1;
		}
		if ((a[j] <= min)&&(a[j] >= min))
		{
			int i;
			double m1 = DBL_MAX;
			for (i = 0; i <= N - 1; i++)
			{
				if ((m1 > this->a[i])&&(this->ind[i] == 1))
				{
					m1 = this->a[i];
				}
			}
			this->min = m1;
		}
		this->a[j] = 0;
	}
}

double & CList::GetS(int j)
{
    if ((j >= N)&&(this->next != NULL))
    {
        return this->next->GetS(j - N);
    }
    else if ((this->next == NULL)&&(j >= N))
    {
        cout << "We get an unreal element";
       // throw -1;
    }
    else if (j < N)
    {
        if (this->ind[j] == 1)
        {
            return this->a[j];
        }
        else
        {
            cout << "We get an unreal element";
        }
    }
	return this->base_value;
}

int CList::GetLength()
{
    if (this->next != NULL)
    {
        return N + this->next->GetLength();
    }
    else
    {
        return N;
    }
}

int CList::GetCount()
{
   int i;
   int res = 0;
   for (i = 0; i < N; i++)
   {
       res += this->ind[i];
   }
   if (this->next != NULL)
   {
       return res + this->next->GetCount();
   }
   else
   {
       return res;
   }
}

void CList::SetLength(int Length)
{
    if (Length > N)
    {
        if (this->next != NULL)
        {
            this->next->SetLength(Length - N);
        }
        else
        {
            this->next = new CList();
            this->next->previous = this;
            this->next->SetLength(Length - N);
        }
    }
}

double & CList::GetX(double x)
{
    if ((x < this->min)||(x > this->max))
    {
        if (this->next != NULL)
        {
            return this->next->GetX(x);
        }
        else
        {
            cout << "ego net";
   //         throw -1;
        }
    }
    else
    {
        int i;
        for (i = 0; i <= N - 1; i++)
        {
            if ((this->ind[i] == 1)&&(x <= this->a[i])&&(x >= this->a[i]))
            {
                return this->a[i];
            }
        }
        if (this->next != NULL)
        {
            return this->next->GetX(x);
        }
        else
        {
            cout << "ego net";
 //           throw -1;
        }
    }
	return this->base_value;
}

SElement CList::Min()
{
    int i;
    int m = -1;
    for (i = 0; i <= N - 1; i++)
    {
        if ((ind[i] == 1)&&(a[i] <= this->min))
        {
            m = i;
            break;
        }
    }
	SElement R;
    if (this->next != NULL)
    {
		SElement Q;
        Q = this->next->Min();
        if (Q.no == -1)
        {
			R.no = m;
			R.where = this;
            return R;
        }
		else if (m != -1)
		{
			if (this->a[m] <= Q.where->a[Q.no])
			{
				R.no = m;
				R.where = this;
				return R;
			}
			else
			{
				R.no = Q.no;
				R.where = Q.where;
				return R;
			}
		}
		else
		{
			R.no = Q.no;
			R.where = Q.where;
			return R;
		}
    }
	else
	{
		R.no = m;
		R.where = this;
		return R;

	}
}


CList CList::Sort() // all good~
{
    CList S;
	CList A(*this);
	while(A.GetCount() != 0)
    {
        SElement M = A.Min();
		if (M.no != -1)
		{
				S.AddEnd(M.where->a[M.no]);
				A.Delete((double)M.where->a[M.no]);
		}
		else
		{
			break;
		}
    }
    return S;
}

void CList::Delete(double x)
{
    int i;
    int j;
	int b = 0;
    for (i = 0; i <= N - 1; i++)
    {
        if ((this->ind[i] == 1)&&(this->a[i] <= x)&&(this->a[i] >= x))
        {
			b = 1;
            j = i;
            this->ind[j] = 0;
            if ((this->a[j] <= max)&&(this->a[j] >= max))
            {
                double m1 = DBL_MIN;
                for (i = 0; i <= N - 1; i++)
                {
                    if ((m1 < this->a[i])&&(this->ind[i] == 1))
                    {
                        m1 = this->a[i];
                    }
                }
                this->max = m1;
            }
            if ((a[j] <= min)&&(a[j] >= min))
            {
                int i;
                double m1 = DBL_MAX;
                for (i = 0; i <= N - 1; i++)
                {
                    if ((m1 > this->a[i])&&(this->ind[i] == 1))
                    {
                        m1 = this->a[i];
                    }
                }
                this->min = m1;
            }
            this->a[j] = 0;
            this->ind[j] = 0;
			return;
		}
    }
	if ((b == 0)&&(this->next != NULL))
	{
		this->next->Delete(x);
	}
	return;
}


CList::CList(const CList &R)
{
    int i;
    this->max = R.max;
    this->min = R.min;
	this->base_ind = R.base_ind;
	this->base_value = R.base_value;
    for (i = 0; i <= N - 1; i++)
    {
        this->a[i] = R.a[i];
        this->ind[i] = R.ind[i];
    }
    if (R.next != NULL)
    {
        this->next = new CList(*(R.next));
        this->previous = NULL;
        this->next->previous = this;
    }
    else
    {
        this->next = NULL;
        this->previous = NULL;
    }
}

CList & CList::operator=(const CList &R)
{
    int i;
    this->max = R.max;
    this->min = R.min;
	this->base_ind = R.base_ind;
	this->base_value = R.base_value;

    for (i = 0; i <= N - 1; i++)
    {
        this->a[i] = R.a[i];
        this->ind[i] = R.ind[i];
    }
    if (R.next != NULL)
    {
        if (this->next != NULL)
        {
            delete this->next;
        }
        this->next = new CList(*(R.next));
        this->previous = NULL;
        this->next->previous = this;
    }
    else
    {
        if (this->next != NULL)
        {
            delete this->next;
        }
        this->next = NULL;
        this->previous = NULL;
    }
    return *this;
}

